import $ from 'jquery';
import 'datatables.net-bs5';
import 'datatables.net-bs5/css/dataTables.bootstrap5.css';

import { Modal } from 'bootstrap';
import  './new'

$("#btn-add-persona").click(function(e){
  e.preventDefault();
  //blockPage();
  const url = $(this).data('url');            
  $.ajax({
      url: url,
      type: "GET",
      data: {
          //var1: valor,
          //var2: "string"
      },
      success: function(data, textStatus, jqXHR){ 
           
          $('#contenedor-modales').html(data);
          const MyModal = new Modal("#modal-add-persona");
          MyModal.show();
          
      },
      error: function(qXHR, textStatus, errorThrow) {
          const message = qXHR.status+' '+qXHR.responseJSON.message;
          //helpers.createNotification('error', message);
          //un//blockPage();
      },
      beforeSend: function( xhr ) {
          //helpers.blockUI();
      },
      complete: function( jqXHR, textStatus ) {
          //helpers.unblockUI();
      }
  });
}); //End agregar item

$('#lista-Persona thead tr')
  .clone(true)
  .addClass('filters')
  .appendTo('#lista-Persona thead');

$('#lista-Persona').DataTable({
  initComplete: function () {
    const api = this.api();
    // For each column
    api
      .columns()
      .eq(0)
      .each(function (colIdx) {
        // Set the header cell to contain the input element
        const cell = $('.filters th').eq(
          $(api.column(colIdx).header()).index()
        );

        //Not add input last cell
        if (colIdx + 1 == api.columns().eq(0).length) {
          $(cell).html('');
          return;
        }


        const title = $(cell).text();

        $(cell).html('<input type="text" placeholder="' + title + '" />');

        $(
          'input',
          $('.filters th').eq($(api.column(colIdx).header()).index())
        ).addClass('form-control');
        // On every keypress in this input
        $(
          'input',
          $('.filters th').eq($(api.column(colIdx).header()).index())
        )
          .off('keyup change')
          .on('keyup change', function (e) {
            e.stopPropagation();

            // Get the search value
            $(this).attr('title', $(this).val());
            const regexr = '({search})'; //$(this).parents('th').find('select').val();

            const cursorPosition = this.selectionStart;

            // Search the column for that value
            api
              .column(colIdx)
              .search(
                this.value != ''
                  ? regexr.replace('{search}', '(((' + this.value + ')))')
                  : '',
                this.value != '',
                this.value == ''
              )
              .draw();

            $(this)
              .focus()[0]
              .setSelectionRange(cursorPosition, cursorPosition);
          });
      });
  },
});
