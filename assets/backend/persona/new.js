$()
$("#contenedor-modales").on("click", "#btn-create-Persona", function () {
  const url = $(this).data('url');
  $.ajax({
    url: url,
    type: "GET",
    data: {
      //var1: valor,
      //var2: "string"
    },
    success: function (data, textStatus, jqXHR) {

      $('#contenedor-modales').html(data);
      const MyModal = new Modal("#modal-add-Persona");
      MyModal.show();

    },
    error: function (qXHR, textStatus, errorThrow) {
      const message = qXHR.status + ' ' + qXHR.responseJSON.message;
      //helpers.createNotification('error', message);
      //un//blockPage();
    },
    beforeSend: function (xhr) {
      //helpers.blockUI();
    },
    complete: function (jqXHR, textStatus) {
      //helpers.unblockUI();
    }
  });
});