<?= "<?php\n" ?>
// ECHO BY JSR
namespace <?= $namespace ?>;

<?= $use_statements; ?>
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\JSR\BaseBundle\Utils\FormErrorsSerializer;
use App\Exception\Database\PrepareDatabaseException;

<?php if ($use_attributes) { ?>
#[Route('<?= $route_path ?>')]
<?php } else { ?>
/**
 * @Route("<?= $route_path ?>")
 */
<?php } ?>



class <?= $class_name ?> extends AbstractController
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

<?= $generator->generateRouteForControllerMethod('/', sprintf('%s_index', $route_name), ['GET']) ?>
<?php if (isset($repository_full_class_name)): ?>
    public function index(<?= $repository_class_name ?> $<?= $repository_var ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            '<?= $entity_twig_var_plural ?>' => $<?= $repository_var ?>->findAll(),
        ]);
    }
<?php else: ?>
    public function index(EntityManagerInterface $entityManager): Response
    {
        $<?= $entity_var_plural ?> = $entityManager
            ->getRepository(<?= $entity_class_name ?>::class)
            ->findAll();

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            '<?= $entity_twig_var_plural ?>' => $<?= $entity_var_plural ?>,
        ]);
    }
<?php endif ?>

<?= $generator->generateRouteForControllerMethod('/new', sprintf('%s_new', $route_name), ['GET', 'POST']) ?>
<?php if (isset($repository_full_class_name) && $generator->repositoryHasAddRemoveMethods($repository_full_class_name)) { ?>
    public function new(Request $request, <?= $repository_class_name ?> $<?= $repository_var ?>): Response
<?php } else { ?>
    public function new(Request $request, EntityManagerInterface $entityManager): Response
<?php } ?>
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        $<?= $entity_var_singular ?> = new <?= $entity_class_name ?>();
        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>, array(
            'action' => $this->generateUrl('<?= $route_name ?>_new'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            try {
                $this->entityManager->persist($<?= $entity_var_singular ?>);
                $this->entityManager->flush();
            } catch (\Throwable $th) {
                PrepareDatabaseException::prepare('Error al guardar el registro para Tipo de Auto.', \get_class($e));
            }

            $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                
            ]);
            $datos = array(
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $<?= $entity_var_singular ?>->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
                
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        
        return $this->render('<?= $templates_path ?>/new.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
    }

<?= $generator->generateRouteForControllerMethod(sprintf('/{%s}', $entity_identifier), sprintf('%s_show', $route_name), ['GET']) ?>
    public function show(<?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        return $this->render('<?= $templates_path ?>/show.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
        ]);
    }

<?= $generator->generateRouteForControllerMethod(sprintf('/{%s}/edit', $entity_identifier), sprintf('%s_edit', $route_name), ['GET', 'POST']) ?>
<?php if (isset($repository_full_class_name) && $generator->repositoryHasAddRemoveMethods($repository_full_class_name)) { ?>
    public function edit(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, <?= $repository_class_name ?> $<?= $repository_var ?>): Response
<?php } else { ?>
    public function edit(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, EntityManagerInterface $entityManager): Response
<?php } ?>
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>, array(
            'action' => $this->generateUrl('<?= $route_name ?>_edit', array(
                '<?= $entity_identifier ?>' => $<?= $entity_var_singular ?>->getId(),
            )),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }

            $this->addFlash('success', 'El <?= $entity_class_name ?> se actualizó correctamente.');

            $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                
            ]);

            $datos = array(
                'code' => JsonResponse::HTTP_OK,
                'id' => $<?= $entity_var_singular ?>->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_OK);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        return $this->render('<?= $templates_path ?>/edit.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
    }

<?= $generator->generateRouteForControllerMethod(sprintf('/{%s}', $entity_identifier), sprintf('%s_delete', $route_name), ['POST']) ?>
<?php if (isset($repository_full_class_name) && $generator->repositoryHasAddRemoveMethods($repository_full_class_name)) { ?>
    public function delete(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, <?= $repository_class_name ?> $<?= $repository_var ?>): Response
<?php } else { ?>
    public function delete(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>, EntityManagerInterface $entityManager): Response
<?php } ?>
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($<?= $entity_var_singular ?>);
                $this->entityManager->flush();

                $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                    
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);

            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }


        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
