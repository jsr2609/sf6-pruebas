
<div id="modal-edit-<?= $entity_command_var ?>" class="modal" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Editar <?= $entity_class_name ?></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                {{ include('<?= $templates_path ?>/_form.html.twig') }}
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal"><i class="fa-solid fa-xmark"></i> Cancelar</button>
                <button type="button" id="btn-update-<?= $entity_command_var ?>" class="btn btn-primary"> <i class="fa-solid fa-floppy-o"></i> Actualizar</button>
            </div>
        </div>
    </div>
</div>
