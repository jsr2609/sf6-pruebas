
<div id="modal-show-<?= $entity_class_name ?>" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Consulta <?= $entity_class_name ?>
          <small class="m-0 text-muted">
            ...
          </small>
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table">
        <tbody>
<?php foreach ($entity_fields as $field): ?>
            <tr>
                <th><?= ucfirst($field['fieldName']) ?></th>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
            </tr>
<?php endforeach; ?>
        </tbody>
    </table>
      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fal fa-times-circle"></i> Cancelar</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#modal-show-<?= $entity_class_name ?>').modal('show');
    $("form").submit(function(){
        //blockPage();
    });
</script>
