<form id="form-delete-{{ entity_name }}" method="post" action="{{ path('<?= $route_name ?>_delete', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}" 
    <input type="hidden" name="_method" value="DELETE">
    <input type="hidden" name="_token" value="{{ csrf_token('delete' ~ <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>) }}">
    
    <button id="btn-delete-{{ entity_name }}" class="btn btn-danger">
        <span>
            <i class="fal fa-trash"></i>
            <span>{{ button_label|default('Delete') }}</span>
        </span>
    </button>
</form>
