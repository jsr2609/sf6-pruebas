<div id="modal-add-<?= $entity_class_name ?>" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Create <?= $entity_class_name ?>
          <small class="m-0 text-muted">
            ...
          </small>
        </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        {{ include('<?= $templates_path ?>/_form.html.twig') }}
      </div>
      <div class="modal-footer">
        <button id="btn-create-<?= strtolower($entity_class_name) ?>" class="btn btn-primary m-btn">
        <span>
            <i class="fal fa-save"></i>
            <span>{{ button_label|default('Save')}}</span>
        </span>
    </button>
        
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fal fa-times-circle"></i> Cancel</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $('#modal-add-<?= $entity_class_name ?>').modal('show');
    
    form = $("form[name={{ form.vars.name }}]");

    form.validate({
    });
    btnCreate = $("#btn-create-<?= strtolower($entity_class_name) ?>");
  btnCreate.on("click", () => {
    form.submit();
  });
    form.submit(function (e) {
        e.preventDefault();
        url = $(this).attr('action');
        formData = new FormData(this);

        if(form.valid()) {
            $.ajax({
            url: url,
            dataType: "JSON",
            type: "POST",
            data: formData,
            processData: false,
            contentType: false,
            success: function (data, textStatus, jqXHR) {
              helpers.createNotification('success', data.message);
              $(location).attr('href',data.url_redirect);
                
            },
            error: function (qXHR, textStatus, errorThrow) {
                const message = qXHR.status+' '+qXHR.responseJSON.message;
                helpers.createNotification('error', message);
                //un//blockPage();
            },
            beforeSend: function( xhr ) {
                btnCreate.prop("disabled", true);
                helpers.blockUI();
            },
            complete: function( jqXHR, textStatus ) {
                btnCreate.prop("disabled", false);
                helpers.unblockUI();
            }
        });
        }
    });
</script>
