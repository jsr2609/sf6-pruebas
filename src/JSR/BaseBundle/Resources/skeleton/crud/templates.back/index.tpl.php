{% extends 'backend/layout/main.html.twig' %}

{% block title %}<?= $entity_class_name ?> Index{% endblock %}
{% block title_module %}
<i class='subheader-icon fal fa-desktop'></i> <?= $entity_class_name ?> <span class='fw-300'>Index </span>
{% endblock %}
{% block title_page %}
    <span class="fw-300"><i class="fal fa-list"></i> Index</span>
{% endblock title_page %}
{% block content %}
    <ul class="list-inline">
        <li class="list-inline-item">
            <button id="btn-add-item" data-href="{{ path('<?= $route_name ?>_new') }}" class="btn btn-primary">
                <span>
                    <i class="fal fa-plus"></i>
                    <span>Nuevo <?= $entity_class_name ?></span>
                </span>
            </button>
        </li>
    </ul>
<div >
    <table id="lista-<?= $entity_class_name ?>" class="table table-bordered  responsive table-hover table-striped w-100">
        <thead class="bg-primary-50">
            <tr>
                <?php foreach ($entity_fields as $key => $field): ?>
                <th  ><?= ucfirst($field['fieldName']) ?></th>
<?php endforeach; ?>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
        {% for <?= $entity_twig_var_singular ?> in <?= $entity_twig_var_plural ?> %}
            <tr>
<?php foreach ($entity_fields as $field): ?>
                <td>{{ <?= $helper->getEntityFieldPrintCode($entity_twig_var_singular, $field) ?> }}</td>
<?php endforeach; ?>
                <td>
                    <div class="btn-group" role="group">
                    <button class="btn-show-item btn btn-primary btn-sm" title="Consultar" data-href="{{ path('<?= $route_name ?>_show', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fal fa-search"></i></button>
                    <button class="btn-edit-item btn btn-info btn-sm" title="Editar" data-href="{{ path('<?= $route_name ?>_edit', {'<?= $entity_identifier ?>': <?= $entity_twig_var_singular ?>.<?= $entity_identifier ?>}) }}"><i class="fal fa-edit"></i></button>
                    </div>
                </td>
            </tr>
        {% else %}
            
        {% endfor %}
        </tbody>
    </table>
    </div>
    <div id="contenedor-modales"></div>

    
{% endblock %}
{% block javascripts %}
{{ parent() }}
<script type="text/javascript">
    $(document).ready(function(){
        // Setup - add a text input to each footer cell
    $('#lista-<?= $entity_class_name ?> thead tr').clone(true).appendTo( '#lista-<?= $entity_class_name ?> thead' );
    numberColumns = $('#lista-<?= $entity_class_name ?> thead tr:eq(1) th').length;
    $('#lista-<?= $entity_class_name ?> thead tr:eq(1) th').each( function (i) {
        if((i+1) < numberColumns) {
            var title = $(this).text();
            $(this).addClass('filterhead');
            $(this).html( '<input type="text" class="form-control" placeholder="'+title+'" />' );

            $( 'input', this ).on( 'keyup change', function () {
                if ( table.column(i).search() !== this.value ) {
                    table
                        .column(i)
                        .search( this.value )
                        .draw();
                }
            } );
        } else {
            $(this).html('');
        }

    } );
    
        var table = $('#lista-<?= $entity_class_name ?>').DataTable({
            
        });
        let hideSearchInputs = (columns) => {
            console.log(columns)
            for (i=0; i<columns.length; i++) {
                if (columns[i]) {
                    $('.filterhead:eq(' + i + ')' ).show();
                } else {
                    $('.filterhead:eq(' + i + ')' ).hide();
                }
            }
        }
        table.on( 'responsive-resize', function ( e, datatable, columns ) {
            hideSearchInputs( columns );

        } );
        $("#btn-add-item").click(function(e){
            e.preventDefault();
            //blockPage();
            url = $(this).attr('data-href');            
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    //var1: valor,
                    //var2: "string"
                },
                success: function(data, textStatus, jqXHR){    
                    $('#contenedor-modales').html(data);
                },
                error: function(qXHR, textStatus, errorThrow) {
                    const message = qXHR.status+' '+qXHR.responseJSON.message;
                    helpers.createNotification('error', message);
                    //un//blockPage();
                },
                beforeSend: function( xhr ) {
                    helpers.blockUI();
                },
                complete: function( jqXHR, textStatus ) {
                    helpers.unblockUI();
                }
            });
        }); //End agregar item
        $("#lista-<?= $entity_class_name ?> ").on("click", "button.btn-edit-item",function(e){
            e.preventDefault();
            //blockPage();
            url = $(this).attr('data-href');            
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    

                },
                success: function(data, textStatus, jqXHR){
                    $('#contenedor-modales').html(data);
                },
                error: function(qXHR, textStatus, errorThrow) {
                    const message = qXHR.status+' '+qXHR.responseJSON.message;
                    helpers.createNotification('error', message);
                    //un//blockPage();
                },
                beforeSend: function( xhr ) {
                    helpers.blockUI();
                },
                complete: function( jqXHR, textStatus ) {
                    helpers.unblockUI();
                }
            });
        }); //End editar-item
        $("#lista-<?= $entity_class_name ?> ").on("click", "button.btn-show-item",function(e){
        
            e.preventDefault();
            //blockPage();
            url = $(this).attr('data-href');            
            $.ajax({
                url: url,
                type: "GET",
                data: {
                    

                },
                success: function(data, textStatus, jqXHR){
                    $('#contenedor-modales').html(data);
                },
                error: function(qXHR, textStatus, errorThrow) {
                    const message = qXHR.status+' '+qXHR.responseJSON.message;
                    helpers.createNotification('error', message);
                    //un//blockPage();
                },
            beforeSend: function( xhr ) {
                helpers.blockUI();
            },
            complete: function( jqXHR, textStatus ) {
                helpers.unblockUI();
            }
            });
        }); //End show-item
    });
</script>
{% endblock %}

