<?= "<?php\n" ?>

namespace <?= $namespace ?>;

use <?= $entity_full_class_name ?>;
use <?= $form_full_class_name ?>;
<?php if (isset($repository_full_class_name)): ?>
use <?= $repository_full_class_name ?>;
<?php endif ?>
use Symfony\Bundle\FrameworkBundle\Controller\<?= $parent_class_name ?>;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Utils\FormErrorsSerializer;

use App\Exception\Database\PrepareDatabaseException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @Route("<?= $route_path ?>")
 */
class <?= $class_name ?> extends <?= $parent_class_name; ?><?= "\n" ?>
{
    
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="<?= $route_name ?>_index", methods={"GET"})
     */
<?php if (isset($repository_full_class_name)): ?>
    public function index(<?= $repository_class_name ?> $<?= $repository_var ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            '<?= $entity_twig_var_plural ?>' => $<?= $repository_var ?>->findAll(),
        ]);
    }
<?php else: ?>
    public function index(): Response
    {
        $<?= $entity_var_plural ?> = $this->getDoctrine()
            ->getRepository(<?= $entity_class_name ?>::class)
            ->findAll();

        return $this->render('<?= $templates_path ?>/index.html.twig', [
            '<?= $entity_twig_var_plural ?>' => $<?= $entity_var_plural ?>,
        ]);
    }
<?php endif ?>

    /**
     * @Route("/new", name="<?= $route_name ?>_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        $<?= $entity_var_singular ?> = new <?= $entity_class_name ?>();
        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>, array(
            'action' => $this->generateUrl('<?= $route_name ?>_new'),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            try {
                $this->entityManager->persist($<?= $entity_var_singular ?>);
                $this->entityManager->flush();
            } catch (\Throwable $th) {
                PrepareDatabaseException::prepare('Error al guardar el registro para Tipo de Auto.', \get_class($e));
            }

            $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                
            ]);
            $datos = array(
                'code' => JsonResponse::HTTP_CREATED,
                'id' => $<?= $entity_var_singular ?>->getId(),
                'message' => 'El registro se creo correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_CREATED);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);
            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
                
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        
        return $this->render('<?= $templates_path ?>/new.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
        
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}/show", name="<?= $route_name ?>_show", methods={"GET"})
     */
    public function show(<?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }
        
        return $this->render('<?= $templates_path ?>/show.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
        ]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}/edit", name="<?= $route_name ?>_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        $form = $this->createForm(<?= $form_class_name ?>::class, $<?= $entity_var_singular ?>, array(
            'action' => $this->generateUrl('<?= $route_name ?>_edit', array(
                '<?= $entity_identifier ?>' => $<?= $entity_var_singular ?>->getId(),
            )),
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $this->entityManager->flush();
            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }

            $this->addFlash('success', 'El <?= $entity_class_name ?> se actualizó correctamente.');

            $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                
            ]);

            $datos = array(
                'code' => JsonResponse::HTTP_OK,
                'id' => $<?= $entity_var_singular ?>->getId(),
                'message' => 'El registro se actualizó correctamente.',
                'url_redirect' => $urlRedirect,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_OK);

        } elseif($form->isSubmitted() && !$form->isValid()) {
            $formErrorsSerializer = new FormErrorsSerializer($form);
            $formErrors = $formErrorsSerializer->serializeFormErrors($form, true, true);

            $datos = array(
                'code' => JsonResponse::HTTP_BAD_REQUEST,
                'message' => 'Se encontraron errores al procesar el formulario',
                'form_errors' => $formErrors,
            );

            return new JsonResponse($datos, JsonResponse::HTTP_BAD_REQUEST);

        } 
        
        return $this->render('<?= $templates_path ?>/edit.html.twig', [
            '<?= $entity_twig_var_singular ?>' => $<?= $entity_var_singular ?>,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{<?= $entity_identifier ?>}", name="<?= $route_name ?>_delete", methods={"POST"})
     */
    public function delete(Request $request, <?= $entity_class_name ?> $<?= $entity_var_singular ?>): Response
    {
        if (
            !$this->isGranted('ROLE_ADMIN') &&
            !$this->isGranted('ROLE_MODULO_ROL')//Agregar el rol para el módulo
        ) {
            throw new AccessDeniedException('Sin permisos para accesar');
        }

        if ($this->isCsrfTokenValid('delete'.$<?= $entity_var_singular ?>->get<?= ucfirst($entity_identifier) ?>(), $request->request->get('_token'))) {
            try {
                $this->entityManager->remove($<?= $entity_var_singular ?>);
                $this->entityManager->flush();

                $urlRedirect =  $this->generateUrl('<?= $route_name ?>_index', [
                    
                ]);

                $datos = [
                    'code' => JsonResponse::HTTP_OK,
                    'message' => 'El registró se eliminó correctamente.',
                    'url_redirect' => $urlRedirect
                ];

                return new JsonResponse($datos, JsonResponse::HTTP_OK);

            } catch (\Throwable $e) {
                PrepareDatabaseException::prepare('No se se puede actualizar el registro', \get_class($e));
            }


        } else {
            throw new \Exception('El token no es valido');
        }
    }
}
