<?php

namespace App\DataFixtures\Faker\Provider;

use App\Service\EncodePasswordService;
use Faker\Provider\Base;

class EncodePasswordProvider extends Base
{
    private EncodePasswordService $encoder;

    public function __construct(EncodePasswordService $encoder)
    {
        $this->encoder = $encoder;
        
    }

    public  function encodePassword(string $plainPassword): string
    {
        return $this->encoder->encode($plainPassword);
    }
}
