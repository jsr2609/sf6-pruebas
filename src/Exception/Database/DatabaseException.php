<?php

declare(strict_types=1);

namespace App\Exception\Database;

use Doctrine\DBAL\Exception\NotNullConstraintViolationException;

class DatabaseException extends \DomainException
{
    public static function savingEntity(string $message): void {
        # code...
        

        throw new self($message);
        
    }
}
