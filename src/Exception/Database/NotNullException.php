<?php

declare(strict_types=1);

namespace App\Exception\Database;

class NotNullException extends \DomainException
{
    public static function prepare(string $message): void {
        throw new self($message);
    }
}
