<?php

declare(strict_types=1);

namespace App\Exception\Database;

class NotFoundException extends \DomainException
{
    public static function prepare(string $message): void {
        throw new self($message);
    }
}
